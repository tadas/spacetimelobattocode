# SpaceTimeLobattoCode

Implementation of space-time DG-SEM schemes using DUNE and DUNE-FEM.

## Installation 

There exist different ways to install DUNE and Assimulo. Here, we only describe the simplest
and most straight forward way to install both, DUNE and Assimulo, which is to
use a conda environment. Then the installation is done in the following way:

Create and activate a new conda environment 
```
conda create -n duneproject
conda activate duneproject
```

Install `Assimulo` and `scipy`
```
conda install -c conda-forge assimulo scipy 
```

Install `dune-fem-dg` including all module dependencies. For this we use pip since no conda package is available yet.

```
pip install -U dune-fem-dg
```

