#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  1 14:49:31 2021

@author: leaversbach
"""

import sys
sys.path.insert(0, "../common")

import numpy
import matplotlib
matplotlib.rc( 'image', cmap='jet' )
from matplotlib import pyplot

#########################################################
## Assimulo imports
#########################################################
#import assimulo.solvers as aso
#from Lobatto_IIIC_Assimulo import Lobatto4ODE
import assimulo.ode as aode
import assimulo.solvers as aso
from Lobatto_IIIC_2s import Lobatto2ODE
from Lobatto_IIIC_3s import Lobatto3ODE
from Lobatto_IIIC_4s import Lobatto4ODE

#########################################################
## DUNE imports
#########################################################
#from dune.grid import structuredGrid as leafGridView
from dune.grid import cartesianDomain
#from dune.alugrid import aluCubeGrid as leafGridView
#from dune.alugrid import aluSimplexGrid as leafGridView
try:
    from dune.spgrid import spAnisotropicGrid as leafGridView
except ImportError:
    from dune.grid import yaspGrid as leafGridView
from dune.common import FieldVector
from dune.grid import reader
from dune.fem import parameter
#from dune.fem.space import dgonb as dgSpace
from dune.fem.space import dglagrangelobatto as dgSpace
from dune.fem.operator import molGalerkin as molGalerkin
from dune.fem.function import uflFunction, integrate
from dune.ufl import Constant
from ufl import TestFunction, TrialFunction, SpatialCoordinate, triangle, FacetNormal
from ufl import dx, ds, grad, div, grad, dot, inner, sqrt, exp, conditional, sin, cos
from ufl import as_vector, avg, jump, dS, CellVolume, FacetArea, atan, pi
from dune.femdg.rk import ssp3, euler
import ufl

# useAssimulo = False
useAssimulo = True

# 3rd order 4-stage Runge-Kutta (R.Alexander)
Stepper = ssp3(4,explicit=False)

# Explicit/Implicit Euler
#Stepper = euler(explicit=False)

time = Constant(0., "time")
dt   = Constant(0.015625,"dt")

Nx = 20
dim = 2
order = 3


class SpatialOperator:
    def __init__(self,form,space,cfl=0.45):
        # create method of lines Galerkin operator from PDE form
        self._op = molGalerkin( form )
        self.cfl = cfl
        self.space = space
        # discrete functions uTmp and vTmp
        self.uTmp = space.interpolate([0], name='uTmp')
        self.vTmp = space.interpolate([0], name='vTmp')
        self.localTimeStepEstimate = [dt.value/self.cfl]

    # v = L[u]
    def apply(self, u, v):
        self._op(u,v)
        self.localTimeStepEstimate = [dt.value/self.cfl]
        return

    # jacobian of L[\bar{u}]
    def jacobian(self, ubar):
        from dune.fem.operator import linear as linearOperator
        return linearOperator(self._op, ubar=ubar).as_numpy

    # v = L[u]
    def __call__(self,u,v):
        self.apply(u,v)
        return

    # make current simulation time known to operator
    def setTime(self, t):
        time.value = t
        self._t  = t

    def stepTime(self,t0, dt0):
        global time
        if hasattr(self._op.model, "time"):
            print(f"Setting time to {self._t} + {t0 * dt}")
            self._op.model.time.value = self._t + t0 * dt
        else:
            time.value = self._t + t0 * dt
        # set time to model time if available
        # since time is not in the form we don't need this here
        #if hasattr(self._op.model,"time"):
        #    print("Model has time")
        #elif hasattr(self._op.model,"t"):
        #    print("Model has t")

    def applyLimiter(self, u):
        pass

    ####################################################
    # rhs function for Assimulo forwarding to apply
    ####################################################
    def rhs(self, t, y):
        """ Function that calculates the right-hand-side. Depending on
            the problem and the support of the solver, this function has
            the following input parameters:

            rhs(t,y)      - Normal ODE
        """
        ## set time for PDE operator
        self.setTime(t)
        # copy content of y into uTmp
        self.uTmp.as_numpy[:] = y[:]
        # apply spatial discretization operator L
        self.apply(self.uTmp, self.vTmp)
        # store result in y
        yn = y.copy()
        yn[:] = self.vTmp.as_numpy[:]
        return yn

    def jac(self, t, y, sw=None):
        # copy content of y into uTmp
        self.uTmp.as_numpy[:] = y[:]
        return self.jacobian( self.uTmp )#.toarray()

parameter.append({"fem.verboserank": 0})

##########################################################
##
## Spatial discretization
##
##########################################################


domain = cartesianDomain([0]*dim, [1]*dim, [Nx]*dim)

#gridView = leafGridView(domain, dimgrid=2 )
gridView = leafGridView(domain)
space    = dgSpace(gridView, order=order)

u    = TrialFunction(space)
v    = TestFunction(space)
n    = FacetNormal(space)
he   = avg( CellVolume(space) ) / FacetArea(space)
hbnd = CellVolume(space) / FacetArea(space)
x    = SpatialCoordinate(space)

epsilon = 0.001
eps = Constant(epsilon,"eps")


center  = as_vector([ 0.5,0.5 ])
x0 = x[0] - center[0]
x1 = x[1] - center[1]

ux = -4.0*x1
uy =  4.0*x0

# diffusion factor
epsilon = 0.001

# transport direction and upwind flux
b =  as_vector([ux,uy])
def u0(x,t):
    sig2 = 0.004
    sig2PlusDt4 = sig2+(4.0*eps*t)
    xq = ( x0*cos(4.0*t) + x1*sin(4.0*t)) + 0.25
    yq = (-x0*sin(4.0*t) + x1*cos(4.0*t))
    return (sig2/ (sig2PlusDt4) ) * exp (-( xq*xq + yq*yq ) / sig2PlusDt4 )

# transport direction and upwind flux
#b =  as_vector([1,1])

#def u0(xp,t):
#    res = 1.
#    for d in range(len(xp)):
#        res *= sin(2.*pi *(xp[d] - t))
#    return res


# b =  as_vector([1.]*(dim))
# #define initial condition/solution at time point t
# def u0(xp,t):
#     res = sin((xp[0]-b[0]*t))*exp(-2*eps*t)
#     for i in range(1,dim):
#         res *= sin((xp[i]-b[i]*t))
#     return res

# upwind (same as LLF in this case)
hatb = (dot(b, n) + abs(dot(b, n)))/2.0

# diffusion factor
eps = Constant(epsilon,"eps")

# penalty parameter for DG scheme
beta = Constant( 10*space.order**2 if space.order > 0 else 1,"beta")

# exact solution
exact = uflFunction(gridView, name="exact", order=3, ufl=u0(x,time))

# d_t u + div( F(u) - eps grad u) = 0
# integration by parts on spatial terms yields
aInternal     = inner(eps*grad(u) -b*u, grad(v)) * dx
advSkeleton   = jump(hatb*u)*jump(v)*dS \
                +(hatb*u + (dot(b,n)-hatb)*exact)*v*ds

# B(u,v) = \int_\Omega grad(u)*grad(v)
#        - \int_\Omega { grad(u) } * [ v ] + [ u ] * { grad(v) }
#        + \int_\Gamma \eta*h^-1 [ u ] * [ v ]
#
# interior skeleton
diffSkeleton  = -eps*inner(jump(u,n),   avg(grad(v)))*dS \
                -eps*inner(avg(grad(u)),jump(v,n))*dS \
                +eps*beta/he*jump(u)*jump(v)*dS
# boundary skeleton -> periodic!
# diffSkeleton += -eps*(u-exact)*dot(grad(v),n)*ds \
#                 -eps*dot(grad(u),n)*v *ds \
#                 +eps*beta/hbnd*(u-exact)*v*ds

#rhs = eps*8.*pi*pi* inner(exact,v) * dx

# minus since we are solving d_t u = L[u]
# which leads in the simplest form to
# unew = uold + dt * L[uold]
# However, in dune-fem we assume that
# d_t u + L[u] = 0
# therefore we implement -L[u] since we solve
# something like d_t u = -L[u]
form = -(aInternal + advSkeleton)
if abs(epsilon) > 0:
    form -= diffSkeleton

## Create right hand side operator
op = SpatialOperator(form, space, cfl=1)



error0 = 0.
eoc = 0


# 3 EOC loops
for i in range(1,2):
    t = 0
    op.setTime( t )
    time.value = t

    # interpolate exact solution onto discrete space
    uh = space.interpolate( exact, name="lobatto")
    #uh.plot()

    # write initial data
    gridView.writeVTK("ic",number=0,celldata=[uh],pointdata=[uh])


    # T
    endTime = 1.

    print('dt:',dt.value)

    # time derivative
    if useAssimulo:
        # uh.plot()
        y0 = numpy.zeros( uh.size )
        y0[:] = uh.as_numpy[:]

        AdvDiff = aode.Explicit_Problem(op.rhs, y0, 0.)
        AdvDiff.name = 'Rotating Pulse'
        AdvDiff.h = dt.value
        AdvDiff.jac = op.jac

        ###Choose which solver to use
        #solver = aso.RungeKutta34(AdvDiff)
        #solver = Lobatto4ODE(AdvDiff)
        solver = Lobatto3ODE(AdvDiff)
        #solver = Lobatto4ODE(AdvDiff)
        #solver = aso.Radau5ODE(AdvDiff)
        solver.report_continuously = True

        solver.atol = 1e-8
        solver.rtol = 1e-8
        solver.h = dt.value/i
        #Option for our Lobatto method
        solver._dynamic_step = False
        solver.krylov = True

        t, y = solver.simulate(endTime)


        # copy last solution back to uh
        uh.as_numpy[:] = y[-1][:]
        print(len(y[-1][:]))
    else:
        stepper = Stepper(op, cfl=op.cfl)

        uh_n = uh.copy()
        while t < endTime:
            stepper( uh, dt.value )
            t += dt.value
            op.setTime( t )
            print(f"time = {t}, dt = {dt.value}")

    #safe result
    gridView.writeVTK(name='LoDGAdvNumSol_order{}'.format(order),pointdata=[uh])
    #print('lenght',len(uh.as_numpy))

    err  = uflFunction(gridView, name="l2err2", order=space.order, ufl=dot(uh-exact, uh-exact))
    error1   = numpy.sqrt(integrate(gridView, err, order=order*2+2))
    # compute L2 error
    #error1= numpy.sqrt( integrate(gridView,dot(uh-exact,uh-exact),order=6))

    # compute EOC
    if i > 0:
        eoc = [ numpy.log(error1/error0) / numpy.log(0.5) ]
    print(f"Step {i}: L2-error {error1} | EOC {eoc}")
    #error0 = error1

    # refine grid to half grid size (delta x)
    gridView.hierarchicalGrid.globalRefine(1)
    # adjust time step size
    dt.value *= 0.25

print('L2 error final step',numpy.sqrt(integrate( gridView, ufl.dot(uh-exact, uh-exact), order=order*2+2 )))
