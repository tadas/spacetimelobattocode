#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  8 10:15:04 2021

@author: leaversbach
"""

import numpy as np
from numpy import pi

from dune.fem.space import dglagrangelobatto as dgSpace
from dune.spgrid import spIsotropicGrid as leafGridView
from dune.fem.function import uflFunction
from dune.grid import cartesianDomain
from dune.fem.scheme import galerkin as solutionScheme
from dune.fem.function import integrate
from dune.generator import algorithm
from ufl import TestFunction, TrialFunction, SpatialCoordinate, FacetNormal
from ufl import dx, ds, grad, dot, inner, sin, exp, cos
from ufl import as_vector, avg, jump, dS, CellVolume, FacetArea, pi, as_matrix

import ufl
from dune.ufl import Constant
from dune.fem import parameter

from dune.fem import threading
threading.use = 6

# enable verbose output on rank 0 (-1 to disable)
parameter.append({"fem.verboserank": 0})

run_count = 0
ele_ns = [4, 8, 16, 32, 64]
#ele_ns = [8]
errorl2 = np.zeros(len(ele_ns))
l2errspace = np.zeros(len(ele_ns))
hsizes = np.zeros(len(ele_ns))

order = 2

timeSteps = 4 #needs to be adapted to dt
t_end = 1.0
dt = t_end/timeSteps

print("dt: ", dt)

dim = 2

time = Constant(0.0,"time")


for ele_n in ele_ns:
    print("dt: ", dt)
    # Mesh and function space.
    gridView = leafGridView(cartesianDomain([0]*dim + [0], [1]*dim + [dt], [ele_n]*dim + [1], periodic=[True]*dim + [False]))

    space    = dgSpace(gridView, order=order, storage='istl')

    #setup for ufl form
    u    = TrialFunction(space)
    v    = TestFunction(space)
    n    = FacetNormal(space)
    he   = avg( CellVolume(space) ) / FacetArea(space)
    hbnd = CellVolume(space) / FacetArea(space)
    x    = SpatialCoordinate(space)

    #### new ###
    # diffusion factor
    epsilon = 0.001
    eps = Constant(epsilon,"eps")

    
    center  = as_vector([ 0.5,0.5 ])
    x0 = x[0] - center[0]
    x1 = x[1] - center[1]

    ux = -4.0*x1
    uy =  4.0*x0

    # transport direction and upwind flux
    b =  as_vector([ux,uy,1.])

    def sol(x,t):
        x0 = x[0] - center[0]
        x1 = x[1] - center[1]
        sig2 = 0.004
        sig2PlusDt4 = sig2+(4.0*eps*t)
        xq = ( x0*cos(4.0*t) + x1*sin(4.0*t)) + 0.25
        yq = (-x0*sin(4.0*t) + x1*cos(4.0*t))
        return (sig2/ (sig2PlusDt4) ) * exp (-( xq*xq + yq*yq ) / sig2PlusDt4 )

    def sol_st(xp):
        x = as_vector([xp[i] for i in range(dim)])
        t = time + xp[dim]
        return sol(x,t)
    '''
    
    b =  as_vector([1.]*(dim+1))
    #define initial condition/solution at time point t
    def sol(xp,t):
        res = sin((xp[0]-b[0]*t))*exp(-2*eps*t)
        for i in range(1,dim):
            res *= sin((xp[i]-b[i]*t))
        return res

    #define exact solution
    def sol_st(xp):
        x = as_vector([xp[i] for i in range(dim)])
        t = time + xp[dim]
        return sol(x,t)
    '''

    M = np.diag(dim*[eps]+[0.])
    D = as_matrix(M)

    # an analytical solution exists
    time.value = 0.0
    exact = uflFunction(gridView, name="exact", order=space.order+2, ufl=sol_st(x))

    # upwind flux (same as LLF in this case)
    hatb = (dot(b, n) + abs(dot(b, n)))/2.0

    # penalty parameter for diffusion term
    beta = Constant( 10*space.order**2 if space.order > 0 else 1,"beta")

    uh_old = space.interpolate(exact, name='uh_old')
    # Set up Dirichlet BC
    #gD = exact
    gD = uh_old

    #Integration by parts yields
    aInternal     = inner(D*grad(u) - b*u, grad(v)) * dx
    advSkeleton   = jump(hatb*u)*jump(v)*dS \
                    +(hatb*u + (dot(b,n)-hatb)*gD)*v*ds

    diffSkeleton  = -inner(jump(u,D*n),avg(grad(v)))*dS \
                    -inner(avg(D*grad(u)),jump(v,n))*dS \
                        +beta/he*inner(jump(u,D*n),jump(v,n))*dS

    # boundary skeleton
    # leave this out because diffusion only acts on the periodic bnd where it's not needed
    #diffSkeleton += -(u-gD)*dot(D*grad(v),n)*ds \
    #                -dot(D*grad(u),n)*v*ds \
    #                +beta/hbnd*inner(D*(u-gD),as_matrix(np.eye(dim+1))*v)*ds

    form = (aInternal + advSkeleton + diffSkeleton)


    def visualize( u_h, time, dt, name='AdvNumSol_order{}_Nx{}_timeSteps{}'.format(order,ele_n,timeSteps), plot = False ):
        gv = u_h.space.gridView
        if gv.dimension < 3 and plot:
            exact.plot()

        dim = gv.dimension - 1
        # get a new grid
        visgrid = leafGridView(cartesianDomain([0]*dim, [1]*dim, [ele_n]*dim))

        # get a new space
        visspace = dgSpace(visgrid, order=order)
        uhEnd = visspace.interpolate( [0], name='ue')

        #for stepwise stuff
        algorithm.run('timeslice', 'timeslice.hh', u_h, uhEnd, dt, dim )
        
        print(time.value)

        xe = SpatialCoordinate(visspace)
        exFct = uflFunction(visgrid, name = "exfct", order = visspace.order+2,
                            ufl = sol(xe, time))

        if dim < 3 and plot:
            uhEnd.plot()

        #save the last time step of the numerical solution in
        visgrid.writeVTK(name, pointdata=[uhEnd, exFct])
        return np.sqrt(integrate( visgrid, ufl.dot(exFct - uhEnd, exFct - uhEnd), order=order*2+2 ))

    ### end visualize ###

    solverParameters =\
        {"newton.tolerance": 1e-8,
         "newton.linear.tolerance": 1e-2,
         "newton.linear.errormeasure": "relative",
         "newton.linear.preconditioning.method": "ilu",
         "newton.linear.preconditioning.iterations": 0,
         "newton.linear.maxiterations":1000,
         #"newton.lineSearch" : "simple",
         "newton.verbose": True,
         "newton.linear.verbose": True
         }

    scheme = solutionScheme(form==0, solver="gmres", parameters=solverParameters)

    uh = space.interpolate(exact, name='uh')

    def project( uh, uhold, comp = dim):
        algorithm.run('shiftToEndTime', 'timeslice.hh', uh, uhold, comp )

    #time.value = 0.

    visualize( uh_old, time, 0., name='AdvIC_order{}_Nx{}_timeSteps{}'.format(order,ele_n,timeSteps) )


    for j in range(timeSteps):
        # solve system
        scheme.solve(target = uh)

        # update simulation time
        time.value += dt

        # project uh(dt) --> uh_old(0)
        project( uh, uh_old)

        print(f"Timestep: {j} , t = {time.value}")

    print("Solved system")
    
    #visualize( uh_old, time, 0., name='AdvIC_order{}_Nx{}_timeSteps{}'.format(order,ele_n,j))
    
    #gridView.writeVTK(name='AdvTimeSlice_order{}_Nx{}_timeSteps{}'.format(order,ele_n,j), pointdata=[uh])

    l2errspace[run_count] = visualize( uh, time, dt )
    errorl2[run_count] = np.sqrt(integrate( gridView, ufl.dot(exact - uh, exact - uh), order=order*2+2 ))
    #print("Error L2: ", errorl2[run_count], l2errspace[run_count])

    hmax = 0.
    for e in gridView.elements:
        vol = np.sqrt(e.geometry.volume)
        hmax = max(hmax, vol)

    hsizes[run_count] = hmax

    run_count += 1
    
    dt *= 0.5
    timeSteps *= 2


#print('L2 error whole time interval',errorl2)
#print('EOC:',np.log(errorl2[0:-1]/errorl2[1:])/np.log(hsizes[0:-1]/hsizes[1:]))
print('L2 error t=T',l2errspace)
#print('EOC:',np.log(l2errspace[0:-1]/l2errspace[1:])/np.log(hsizes[0:-1]/hsizes[1:]))
#print(np.log(hsizes[0:-1]/hsizes[1:]))
#print(hsizes)

