import sys
sys.path.insert(0, "../common")

###############################################################
# Python imports
###############################################################
import numpy as np
from numpy.compat import long
import time

###############################################################
# Dolfin DG import
#
# from https://bitbucket.org/nate-sime/dolfin_dg/
###############################################################
import ufl
from ufl import as_vector
import dune.ufl
from dune.ufl import BoundaryId

###############################################################
# Local import
###############################################################
#from steuleroperator import SpaceTimeCompressibleEulerOperator
from dolfin_dg.operators import SpaceTimeCompressibleEulerOperator
from eulerexact import spacetimeexact
from dolfin_dg import DGDirichletBC

###############################################################
# DUNE imports
###############################################################
from dune.fem.space import dglagrangelobatto as dglagrangelobatto
from dune.spgrid import spIsotropicGrid as leafGrid
from dune.fem.view import adaptiveLeafGridView as advGridView
from dune.fem.function import uflFunction
from dune.grid import cartesianDomain
from dune.fem.scheme import galerkin as solutionScheme
from dune.fem.function import integrate, partitionFunction
from dune.generator import algorithm
from dune.fem import parameter
from dune.ufl import Constant
from dune.common import comm
from dune.grid import PartitionType

from eulerexact import spacetimeexact
################################################################
startTime = time.time()

# spatial dimension
dim = 2

run_count = 0

# number of elements in each direction for each run
ele_ns = [4,8,16 ] #,32] #64

# domain coordinates
domain_lower = [0]*dim
domain_upper = [1]*dim

#ele_ns = [40]
errorl2 = np.zeros(len(ele_ns))

#from dune.fem import threading
#threading.use = 6

parameter.append({"fem.verboserank" : 0 })

order = 3


timeSteps = ele_ns[0]
t_end = 0.6

t = Constant(0.0,"time")
mainRank = (comm.rank == 0)

for ele_n in ele_ns:
    # compute time step size
    dt = t_end/timeSteps

    # domain and grid
    domain = cartesianDomain(domain_lower + [0], domain_upper + [dt], [ele_n]*dim + [1], periodic=[True]*dim + [False], overlap=1)
    T_h = advGridView( leafGrid(domain) )

    # dimRange = number of equations (here dim+2)
    # create discrete function space
    V_h = dglagrangelobatto(T_h, dimRange = dim+2, order = order)
    nDofs = long(T_h.comm.sum( V_h.size ))
    nElemLoc = 0
    for e in T_h.elements:
        if e.partitionType == PartitionType.Interior:
            nElemLoc += 1
    nElem = long(T_h.comm.sum( nElemLoc ))

    if mainRank:
        print(f"#Elem: {nElem}")
        print(f"#DoFs: {nDofs}")

    # Set up Dirichlet BC
    x = ufl.SpatialCoordinate(V_h)
    t.value = 0.0
    # exact solution (from eulerexact.py)
    u_ex = uflFunction(T_h, name = "u_ex", order = order,ufl = spacetimeexact(x, t))

    # create discrete solution
    u_h = V_h.interpolate(u_ex, name='u_h')
    u = ufl.TrialFunction(V_h)
    v = ufl.TestFunction(V_h)

    u_h_old = V_h.interpolate(u_ex, name='u_h_old')

    L   = SpaceTimeCompressibleEulerOperator(T_h, V_h, DGDirichletBC(ufl.ds, u_h_old))
    L_h = L.generate_fem_formulation(u, v, ufl.dx, ufl.dS)

    ## visualization routine evaluating u_h(t+dt) and store
    ## in discrete function of lower dimension
    def visualize( u_h, t, dt, name='EulerNumSol_order{}_Nx{}_timeSteps{}'.format(order,ele_n,timeSteps), plot = False ):
        from eulerexact import exact

        T_h = u_h.space.gridView
        if T_h.dimension < 3 and plot:
            u_h.plot()

        dim = T_h.dimension - 1
        # get a new grid
        T_spc = leafGrid(cartesianDomain(domain_lower, domain_upper, [ele_n]*dim, overlap=0))

        # get a new space
        V_spc = dglagrangelobatto(T_spc, dimRange=V_h.dimRange, order=V_h.order)
        u_h_spc = V_spc.interpolate( [0]*V_spc.dimRange, name='u_h_spc')

        # evaluate u_h(t+dt) and store in u_h_spc
        algorithm.run('timeslice', '../common/timeslice.hh', u_h, u_h_spc, dt, dim )

        x_s = ufl.SpatialCoordinate( V_spc )
        exFct = uflFunction(T_spc, name = "exfct", order = order,
                            ufl = exact(x_s, t))

        # matplotlib plotting only for 1d and 2d
        if dim < 3 and plot:
            u_h_spc.plot()

        #save the last time step of the numerical solution in
        T_spc.writeVTK(name, pointdata=[u_h_spc, exFct, partitionFunction(T_spc)])
        return np.sqrt(integrate( T_spc, ufl.dot(exFct - u_h_spc, exFct - u_h_spc), order=V_h.order*2+2 ))

    ### end visualize ###

    solverParameters =\
       {"newton.tolerance": 1e-6,
        "newton.linear.tolerance": 1e-2,
        "newton.linear.errormeasure": "relative",
        "newton.linear.preconditioning.method": "jacobi",
        #"newton.linear.preconditioning.method": "ssor",
        #"newton.linear.preconditioning.method": "hypre",
        #"petsc.preconditioning.hypre.method" : "boomer-amg",
        #"newton.linear.petsc.blockedmode": False,
        #"newton.linear.preconditioning.method": "sor",
        #"newton.linear.preconditioning.method": "jacobi",
        "newton.linear.preconditioning.iterations": 3,
        "newton.linear.maxiterations":10000,
        "newton.linear.threading":False,
        "newton.verbose": True,
        "newton.linear.verbose": True
        }

    scheme = solutionScheme([L_h == 0], solver=("istl","gmres"), parameters=solverParameters)

    def project( u_h, u_h_old, comp = dim):
        algorithm.run('shiftToEndTime', '../common/timeslice.hh', u_h, u_h_old, comp )

    # set t = 0
    t.value = 0.

    # visualize initial solution
    visualize( u_h_old, t, 0., name='EulerIC_order{}_Nx{}_timeSteps{}'.format(order,ele_n,timeSteps) )

    if mainRank:
        print("dt: ", dt)

    ## time loop
    for j in range(timeSteps):
        # solve system
        scheme.solve(target = u_h)

        # update simulation time
        t.value += dt

        # project u_h(dt) --> u_h_old(0)
        project( u_h, u_h_old )

        if mainRank:
            print(f"Timestep: {j} , t = {t.value}")
    ## end time loop

    if mainRank:
        print(f"Solved the system! T={t.value}")

    #timeSteps *= 2

    #T_h.writeVTK('EulerNumSolTimeSlice_order{}_Nx{}_timeSteps{}'.format(order,ele_n,timeSteps), pointdata=[u_h, u_ex, partitionFunction(T_h)])

    l2errspace = visualize( u_h, t, dt )
    errorl2[run_count] = np.sqrt(integrate( T_h, ufl.dot(u_ex - u_h, u_ex - u_h), order=V_h.order*2+2 ))
    if mainRank:
        print("Error L2: ", errorl2[run_count], l2errspace)
        print("EOC   L2: ", np.log(errorl2[run_count-1]/errorl2[run_count])/np.log(2.) if run_count > 0 else None)

    run_count += 1

    dt *= 0.5
    timeSteps *= 2

endTime = time.time()
localRunTime = (endTime-startTime)
minRunTime = comm.max( localRunTime )
maxRunTime = comm.min( localRunTime )

# print all EOCs
if mainRank:
    print("EOC   L2: ", np.log(errorl2[0:-1]/errorl2[1:])/np.log(2.))
    print("Runtime (min/max):  ", minRunTime, maxRunTime)
