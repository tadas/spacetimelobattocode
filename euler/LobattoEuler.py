#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.insert(0, "../common")

#########################################################
## Python imports
#########################################################

import numpy as np
from numpy import pi

import matplotlib
matplotlib.rc( 'image', cmap='jet' )
#from matplotlib import pyplot

#########################################################
## Assimulo imports
#########################################################
import assimulo.ode as aode
import assimulo.solvers as aso
from Lobatto_IIIC_2s import Lobatto2ODE
from Lobatto_IIIC_3s import Lobatto3ODE
from Lobatto_IIIC_4s import Lobatto4ODE

#########################################################
## Dolfin DG imports
#
# from https://bitbucket.org/nate-sime/dolfin_dg/
#########################################################

import dolfin_dg as dg
from dolfin_dg import CompressibleEulerOperator

#########################################################
## DUNE imports
#########################################################

from dune.grid import cartesianDomain
try:
    from dune.spgrid import spIsotropicGrid as leafGridView
except ImportError:
    from dune.grid import yaspGrid as leafGridView
    print('using yaspGrid')
from dune.fem import parameter
from dune.fem.space import dglagrangelobatto
from dune.fem.function import uflFunction, integrate
from dune.ufl import Constant
import ufl
from ufl import dot
from dune.femdg.rk import ssp3, euler

##########################################################
## Local imports
##########################################################
from spatialoperator import SpatialOperator
from eulerexact import exact
#from vortex import exact, domain


parameter.append({"fem.verboserank":0})

#useAssimulo = False
useAssimulo = True

# 3rd order 4-stage Runge-Kutta (R.Alexander)
Stepper = ssp3(4,explicit=True)

# Explicit/Implicit Euler
#Stepper = euler(explicit=False)

##########################################################
##
## Spatial discretization
##
##########################################################

run_count = 0
#ele_ns = [4, 8, 16, 32, 64]
ele_ns = [20]
EOC = []

order = 3

Nx = 20

dim = 2

# domain coordinates
domain_lower, domain_upper = domain(dim)

time = Constant(0., "time")
dt   = Constant(0.05,"dt")

# Mesh and function space.
domain = cartesianDomain(domain_lower, domain_upper,[Nx]*dim, periodic=[True]*dim)
gridView  = leafGridView(domain)

# dimRange = number of equations (here dim+2)
V_h = dglagrangelobatto(gridView, dimRange = dim+2, order=order)

x = ufl.SpatialCoordinate(V_h)

u_ex = uflFunction(gridView, name = "u_ex", order = order, ufl = exact(x, time))

u_h = V_h.interpolate(u_ex)
u = ufl.TrialFunction(V_h)
v = ufl.TestFunction(V_h)

# create PDE model
L = CompressibleEulerOperator(gridView, V_h, dg.DGDirichletBC(ufl.ds, u_ex))
L_h = -L.generate_fem_formulation(u, v, ufl.dx, ufl.dS)

# create Method of Lines spatial operator based on the PDE operator L_h
F = SpatialOperator(L_h, V_h, time, dt, cfl=1)

#############


error0 = 0.
eoc = 0

# EOC loops
for i in range(len(ele_ns)):
    t = 0
    F.setTime( t )
    time.value = t

    # interpolate exact solution onto discrete space
    u_h = V_h.interpolate( u_ex, name="lobatto")

    # create VTK writer
    vtk = gridView.sequencedVTK('EulerLoDG_order{}_Nx{}_dt{}'.format(order,Nx,dt.value), pointdata=[u_h,u_ex], celldata=[u_h,u_ex], subsampling=2)

    # write initial data
    vtk()

    # T
    endTime = 3.0

    print('dt:', dt.value)

    # time derivative
    if useAssimulo:
        # u_h.plot()
        y0 = np.zeros( u_h.size )
        y0[:] = u_h.as_numpy[:]

        Euler = aode.Explicit_Problem(F.rhs, y0, 0.)
        Euler.name = 'Smooth bubble'
        Euler.h = dt.value
        Euler.jac = F.jac

        ###Choose which solver to use
        #solver = aso.RungeKutta34(Euler)
        #solver = Lobatto2ODE(Euler)
        solver = Lobatto3ODE(Euler)
        #solver = Lobatto4ODE(Euler)
        solver.report_continuously = True

        solver.atol = 1e-5
        solver.rtol = 1e-5
        # solver.h = dt.value
        #Option for our Lobatto method
        solver._dynamic_step = False
        solver._sparseJ = True

        t, y = solver.simulate(endTime,1,[endTime]) #only y[-1.:] saved
        print(len(t))
        print(len(y))

        # copy last solution back to u_h
        u_h.as_numpy[:] = y[-1,:]
    else:
        stepper = Stepper(F, cfl=F.cfl)
        saveInterval = endTime / 10.
        saveStep = saveInterval
        while t < endTime:
            stepper( u_h, dt.value )
            t += dt.value
            F.setTime( t )
            if t > saveStep:
                print(f"time = {t}, dt = {dt.value}")
                # write data
                vtk()
                saveStep += saveInterval

    #safe result
    vtk()

    gridView.writeVTK('EulerLoDG_order{}_Nx{}_dt{}New'.format(order,Nx,dt.value), pointdata=[u_h, u_ex])

    # compute L2 error
    error1 = np.sqrt( integrate(gridView,dot(u_h-u_ex,u_h-u_ex),order=6) )

    # plot solution
    if gridView.dimension < 3:
        u_h.plot()

    # compute EOC
    if i > 0:
        eoc = [ np.log(error0/error1) / np.log(2.) ]
    EOC.append(eoc)
    print(f"Step {i}: L2-error {error1} | EOC {eoc}")
    error0 = error1

    # refine grid to half grid size (delta x)
    gridView.hierarchicalGrid.globalRefine(1)
    # adjust time step size
    dt.value *= 0.25

# print all EOCs
print("EOC   L2: ", EOC)

