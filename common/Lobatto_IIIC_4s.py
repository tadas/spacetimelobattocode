#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  6 14:37:23 2021

@author: leaversbach
"""


import sys #for min float number
import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as alg
from scipy.sparse.linalg import splu
from scipy.sparse import bmat
from scipy.sparse import diags as diagonal
from scipy.sparse import csc_matrix
from abc import ABC, abstractclassmethod
from scipy.linalg import lu_factor,lu_solve

from assimulo.explicit_ode import Explicit_ODE
from assimulo.ode import ID_PY_OK,NORMAL



class RadauError(Exception):
    def __init__(self,expr):
        self.expr = expr
    def __str__(self):
        return self.expr


class RKS4CBase(ABC,Explicit_ODE): #Runge-Kutta S=4 Complex eigen pair Base class

    rtol = 1.e-6 #realative tolerance    
    atol = 1.e-6 #absolute tolerance
    tol  = 1.e-6 #tolerance for newton iteration
    kappa = 1e-1 #The kappa value used for newton stopping in eq (15)
    maxit = 50 #maximum number of newton iterations
    maxsteps=10000000 #maximum number of steps
    r=1 #r is the ratio between the previouse step size and the current

    #used for stoping after single newton itt
    Uround = sys.float_info.min 
    
    #the bounds to decide weather or not to adjust the step size eq (41)
    c1 = 0.95
    c2 = 1.2


    def __init__(self,problem):
        """
        Initiates the solver.
        
            Parameters::
            
                problem     
                            - The problem to be solved. Should be an instance
                              of the 'Explicit_Problem' class.
        """
        Explicit_ODE.__init__(self, problem) #Calls the base class 
        
        #Solver options
        try:
            self.options['h'] = problem.h
            self.h = problem.h
        except:
            self.options['h'] = 1e-4
            self.h = 1e-4
        self._sparceJ = True
        self._dynamic_step = False

        #Statistics
        self.statistics["nsteps"] = 0
        self.statistics["nfcns"] = 0
        self.num_jac_evals = 0
        self.num_lu_decomps = 0

        #method parameter:
        self.y_dim = len(self.problem.y0)
        self.RTol = np.array([self.rtol]*self.y_dim)
        self.ATol = np.array([self.atol]*self.y_dim)
        self._3ATol = np.hstack((self.ATol,self.ATol,self.ATol,self.ATol))
        self._3RTol = np.hstack((self.RTol,self.RTol,self.RTol,self.RTol))

        #internal flags
        self._first = True
        self._recomp_jac = True
        self._recomp_LHS = True
        self._newt_fail_flag = False

        #internal values
        self.J = None
        self._lu = None
        

    #set and get for sparce Jacobian, need to redifine the lu decompose and
    #lu solve methods here
    def _set_sparce(self,sp):
        self.options["sparse"] = bool(sp)
        if sp:
            def lu_decomp_s(LHS):
                lu1 = splu(LHS[0])
                lu2 = splu(LHS[1])
                return (lu1,lu2)
            self._LUDecomp = lu_decomp_s

            def lu_solve_s(lu,RHS):
                n = RHS.shape[0]//4
                dwk = np.zeros_like(RHS)
                dwk[:2*n] = lu[0].solve(RHS[:2*n])
                dwk[2*n:] = lu[1].solve(RHS[2*n:])
                return dwk
            self._LUSolve = lu_solve_s
        else:
            def lu_decomp_d(LHS):
                lu1 = lu_factor(LHS[0])
                lu2 = lu_factor(LHS[1])
                return (lu1,lu2)
            self._LUDecomp = lu_decomp_d

            def lu_solve_d(lu,RHS):
                n = RHS.shape[0]//4 
                dwk = np.zeros_like(RHS)
                dwk[:2*n] = lu_solve(lu[0],RHS[:2*n])
                dwk[2*n:] = lu_solve(lu[1],RHS[2*n:])
                return dwk
            self._LUSolve = lu_solve_d
            
    def _get_sparce(self):
        return self.options["sparse"]
    _sparceJ = property(_get_sparce,_set_sparce)

    #set and get for h parameter, when h changes we also want to recompute the
    #LHS
    def _set_h(self,h):
        self.options["h"] = float(h)
    def _get_h(self):
        return self.options["h"]
    h=property(_get_h,_set_h)


    def integrate(self,t,y,tf,opts):
        """
        _integrates (t,y) values until t > tf
        """
        self.h = min(self.h,abs(tf-t))

        tres = []
        yres = []

        z0 = np.zeros((self.y_dim*4))

        #lists for storing results
        for i in range(self.maxsteps):
            if t>= tf:
                break
            
            t,y = self._step(t,y,z0)
            
            self._first = False
            
            self.t = t #used for Radau error approximation

            #store the results in array to be returned when done
            tres.append(t)
            yres.append(y.copy())
            
            
            if self.h > np.abs(tf-t):
                h_old = self.h / self.r
                self.h = np.abs(tf-t)
                self.r = self.h/h_old
                self._recomp_LHS=True
                
            if abs(self.r-0.5)<0.1: #TODO: this needs to be fixed?
                #if we are to close to the explosion point, change value
                print('got here')
                h_old = self.h/self.r
                self.r = 0.35 #TODO: fix this parameter
                self.h = self.r * h_old
                self._recomp_LHS=True
                
            if abs(self.h) < self.Uround:
                break

        else:
            raise RadauError(f"final time not reached within maximum number of steps ({self.maxsteps})")
        return ID_PY_OK, tres, yres
        

    def _step(self,t,y,z0):
        #update statistics
        self.statistics["nsteps"] += 1
        #recomp jacobian if the flag is set
        if self._recomp_jac:
            self.J = self._jacobian(t,y)
            self._recomp_jac = False
            self._recomp_LHS = True
        
        #newton iteration
        y,h,total1, total2, total3, total4 = self._newton(self.J,y.copy(),t,z0)
        
        #!!! here printing stuff in case all stage values need to be available
        '''
        f = open('results_{}.txt'.format(h),'a')  # w : writing mode  /  r : reading mode  /  a  :  appending mode
        f.write('{}\n'.format(total1))
        f.write('{}\n'.format(total2))
        f.write('{}\n'.format(total3))
        f.write('{}\n'.format(total4))
        f.close()
        '''
        
        return t + h, y

    def _newton(self,J,y0,t,z0=None):

        #handy dimension variable
        n  = self.y_dim

        #setup newton starting value
        zk = z0 if type(z0) is np.ndarray else np.zeros(3*n)
        wk = self._zTOw(zk)

        #newton stopping variables
        ndzk = None #Norm of delta Zk
        theta_k = None #Theta_k

        #setup LHS and lu decompose if needed
        if self._recomp_LHS or self._lu == None:
            LHS = self._compLHS(J)
            self._lu = self._LUDecomp(LHS)
            self.num_lu_decomps += 1
            self._recomp_LHS = False

        for i in range(1,self.maxit):
            #compute the RHS
            RHS = self._compRHS(wk,zk,y0)
            
            #compute the delta W_k
            dwk = self._LUSolve(self._lu,RHS) #eq (17a)
            
            #convert delta W_ktodelta Z_k
            dzk = self._wTOz(dwk)
            
            #increment wk and zk with the corresponding delta values
            wk += dwk #eq (17b)
            zk += dzk 
            
            #---------newton stopping condition-----------
            
            #for sci we require the max of z_k and z_{k-1} termwise
            diff = dzk.copy()
            diff[diff>0] = 0 #wherever the value has increased we want to keep
            sci = self._3ATol + (zk-diff)*self._3RTol
            
            #norm of delta zk
            ndzk = self._norm(dzk,sci)
            
            #declare the eta_k variable so it is outside the if scope
            eta_k = None
            if i == 1 :#first step in newton
                if '_eta' in self.__dict__:
                #one step stopping
                    eta_k = max(self._eta,self.Uround)**.8
                else:
                #first time step, needs at least 2 newton
                    ndzkm1 = ndzk
                    continue
            else:
                #compute theta_k and eta_k
                theta_k = ndzk/ndzkm1 # eq (14)
                eta_k   = theta_k/(1-theta_k) #eq (15)

                #check if we are diverging
                if theta_k > 1:
                    
                    if not self._dynamic_step:
                        raise RadauError(f'newton iteration diverged')
                    
                    #adjust step size
                    self.h /= 2
                    self.r /= 2
                    
                    #if r is to close to 0.5 adjust it down further,
                    #this is due to explosion of values in error estimation
                    #TODO: move this into the set h function.
                    if (self.r-0.5)<0.1:
                        print('got here?')
                        self.h *= 0.35 /self.r
                        self.r = 0.35
                    
                    #recompute the jacobian
                    self.J = self._jacobian(t,y0)
                    self._recomp_LHS = True
                    
                    #re-run newton
                    return self._newton(self.J,y0,t,z0)

            #test if newton has converged, eq (14)
            if eta_k * ndzk <= self.kappa * self.tol:
                #save old eta for singe step stopping
                self._eta = eta_k
                
                #new y value
                y1 = self._zTOy(y0,zk) 
                #the h value that was used
                h = self.h
                
                #------determine approximate error---------
 
                #return result and the h used.
                return y1, h, self._znewy1(y0,zk)[0],y1[0],self._znewy2(y0,zk)[0],self._znewy3(y0,zk)[0]
                
            
            #update the norm for newton stopping condition
            ndzkm1 = ndzk
        
        else:
            #if we do not converge within number of newton itteration steps
            if not self._newt_fail_flag:
                print("newton fail")
                self._newt_fail_flag = True
                #rerun with new Jacobian if not done yet
                self.J = self._jacobian(t,y0)
                self._recomp_LHS = True
                #re-run newton
                return self._newton(self.J,y0,t,z0)
            #otherwise raise
            raise RadauError(f'newton iteration did not converge after {self.maxit} iterations')

    
    @abstractclassmethod
    def _adjust_h(self,err_mag,TOL = 1e-5,step_accept = True,itt = 0):
        return np.Inf
        
    
    def _norm(self,vec,sc):
        #calculates the norm of a vector according to eq (38)
        return np.sqrt((1/vec.shape[0])*np.sum((vec/sc)**2))


    def _compRHS(self,wk,zk,y):  
        #print('RHS')
        #computes the RHS of eq (17a)

        #dimenson of system
        n = self.y_dim
        
        #initial term:
        term1 = -np.array(2*n*[self.alpha1/self.h] + \
                        2*n*[(self.alpha2)/self.h])* wk
        
        term1[:n] += self.beta1/self.h * wk[n:2*n] 
        term1[n:2*n] -= self.beta1/self.h * wk[:n]
        term1[2*n:3*n] += self.beta2/self.h * wk[3*n:] 
        term1[3*n:] -= self.beta2/self.h * wk[2*n:3*n]

        #function term
        fcn = self.problem.rhs
        f = np.array([fcn(self.t+self.c[0]*self.h,y + zk[:n]),
            fcn(self.t+self.c[1]*self.h,y + zk[n:2*n]),
            fcn(self.t+self.c[2]*self.h,y + zk[2*n:3*n]),
            fcn(self.t+self.c[3]*self.h,y + zk[3*n:])])
        prod = (self.TI @ f).flatten()

        self.statistics["nfcns"] += 4

        return term1 + prod 

    def _compLHS(self,J):
        #construction of the LHS of eq (17a)

        #TODO: for more efficiency construct the blocks in sparce format
        #instead of converting J to dense and then the blocks back to sparce
        
        A1  = csc_matrix(J) 
        A1 *= -1.0 
        diag = A1.diagonal()
        diag[:] += self.alpha1/self.h
        A1.setdiag( diag )
        diag[:] = self.beta1/self.h
        B11 = diagonal( -diag )
        B21 = diagonal(  diag )
        spmat1 = bmat( [[A1, B11], [B21, A1]], format="csc" )
        
        A  = csc_matrix(J) #
        A *= -1.0 # we need self.alpha/self.h - J
        diag = A.diagonal()
        diag[:] += self.alpha2/self.h
        A.setdiag( diag )
        diag[:] = self.beta2/self.h
        B1 = diagonal( -diag )
        B2 = diagonal(  diag )
        spmat2 = bmat( [[A, B1], [B2, A]], format="csc" )
            
        return (spmat1,spmat2)
    

    def _jacobian(self,t,y):
        """
        calculates the jacobian
        """
        if self.problem_info['jac_fcn']:
            self.num_jac_evals += 1
            J = self.problem.jac(t,y)
            self._recomp_jac = False
            return J
        else:
            raise RadauError('The current implimentation cannot calculate an approximate jacobian yet,' +
                    'hence a jacobian function must be given in the problem')

    def _zTOw(self,z):
        #TODO: this might one returns return directly...
        #convert from z to w
        w = (self.TI@ z.reshape((4,-1))).flatten()
        return w

    def _wTOz(self,w):
        #convert from w to z
        z = (self.T @ w.reshape((4,-1))).flatten()
        return z

    def _zTOy(self,y,z):
        #convert from z to y
        y = y + self.d @ z.reshape((4,-1))
        return y
    
    def _znewy1(self,y,z):
        #convert from z to y
        y = y + np.array([1,0,0,0]) @ z.reshape((4,-1))  ### !!!
        return y
    
    def _znewy2(self,y,z):
        #convert from z to y
        y = y + np.array([0,0,1,0]) @ z.reshape((4,-1))  ### !!!
        return y
    
    def _znewy3(self,y,z):
        #convert from z to y
        y = y + np.array([0,1,0,0]) @ z.reshape((4,-1))  ### !!!
        return y

    def print_statistics(self, verbose=NORMAL):
        """
        Prints the run-time statistics for the problem.
        """
        Explicit_ODE.print_statistics(self, verbose) #Calls the base class
        self.log_message('\nSolver options:\n',                                              verbose)
        self.log_message(' Solver                  : 4-stage Lobatto IIIC',                  verbose)
        self.log_message(' Number of J evaluations : ' + str(self.num_jac_evals),    verbose)
        self.log_message(' Number of LU decompos   : ' + str(self.num_lu_decomps),   verbose)
        self.log_message('\n\n\n',verbose)
        

class Lobatto4ODE(RKS4CBase):
        
    #-------- Method constants -----------
    #the accuracy might not be good enough here
    # T matrix
    T = np.array([
       [-0.476834448954395, 0.389372188446217, 0.337149569691852, -0.0768854862649371],
       [0.199416955944611, -0.310517818943981,  -0.00642273194053416, 0.092024731028135],
       [0.241100104651109,  0.573615508653106,  0.0893481566474612, 0.452109380217202],
       [-0.686250584802386,  0.829448092838046,  -0.314624938113068,  1.28868615606748]])
    
    # T inverse matrix
    TI= np.array([
        [-0.773924666194085, 0.0229850223665799,  1.23183645763175, -0.479980000770784],
        [-0.413559766261928,  -2.53580013215033, 0.862945340895804, -0.146339821353887],
        [  2.45234418373772,   3.53277358729325, 0.813830668863258, -0.391478825953491],
        [  0.45277800100903,   2.50688402644078, 0.299243958050725,  0.518998147817944]])

    #eigen values gamma, alpha and betta
    alpha1: float = 2.22098003298981
    beta1: float = -4.16039144550693
    alpha2: float = 3.77901996701019
    beta2: float = -1.38017652427284
    
    # c,d and b (see paper for reference)    
    c = np.array([0.,(5-np.sqrt(5))/10,(5+np.sqrt(5))/10,1.])
    d = np.array([0,0,0,1.]) 
    b = np.array([1/12,5/12,5/12,1/12.])
    
    #special flag used when wanting to compair error estimates
    _do_comp_err = False #set this to true to compair the error
    
    def __init__(self,problem):
        RKS4CBase.__init__(self,problem)

        #list for storing error compairison if flag is set
        self.err_comp = []

    #overwrite z to y conversion to be more efficient
    def _zToy(self,y,z):
        return y + z[2*self.y_dim:]

    
    def _adjust_h(self,err_mag,TOL = 1e-5,step_accept = True,itt = 0):
        #function to adjust the step size
        fac = 0.9 * (2*self.maxit+1)/(2*self.maxit+itt)
        print('adjust_h needed')
        return fac * self.h * err_mag ** (-1/6)
    
        

if __name__ == '__main__':
    #-----example usage-----
    from assimulo.ode import Explicit_Problem
    import matplotlib.pyplot as plt

    #define the problem
    g : float = 9.81 
    l : float = 0.5 
    theta : float = np.pi/2.
    def rhs(t,y): 
        return np.array([ 
            y[1], 
            np.sin(y[0])*g/l 
            ]) 

    def J(t,y): 
        return np.array([ 
            [0, 1], 
            [np.cos(y[0])*g/l,0] 
            ]) 

    #--helper functions for calculating period
    def arith_geo_mean(a,b):
        while True:
            a,b = (a+b)/2, np.sqrt(a*b)
            yield a,b

    from itertools import islice
    def elliptic_integral(k,tol=1e-5,maxiter=100):
        a_0,b_0 = 1.,np.sqrt(1-k**2)
        for a,b in islice(arith_geo_mean(a_0,b_0),maxiter):
            if abs(a-b) < tol:
                return np.pi /(2*a)
        else:
            raise Exception('Algorithm did not converge')

    periode = 4*np.sqrt(l/g) * elliptic_integral(np.sin(theta/2),tol=1e-10)

    #----Initialize Problem
    pend_model = Explicit_Problem(rhs,y0=np.array([theta,0.]))
    pend_model.name = 'Pendulum simulation'
    pend_model.jac = J
    pend_model.h = 0.01

    #----Initialize Solver
    sim = Lobatto4ODE(pend_model)
    sim.h = 0.016
    sim._do_comp_err = False
    sim._dynamic_step = False
    sim.maxit=10
    #----Simulate the solver
    t,y = sim.simulate(periode)
    #----Plot results
    sim.plot()
    plt.show()

