import sys #for min float number
import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as alg
from scipy.sparse.linalg import splu
from scipy.sparse import bmat
from scipy.sparse import diags as diagonal
from scipy.sparse import csc_matrix
from abc import ABC, abstractclassmethod
from scipy.linalg import lu_factor,lu_solve
from scipy.sparse.linalg import gmres, spilu, LinearOperator

from assimulo.explicit_ode import Explicit_ODE
from assimulo.ode import ID_PY_OK,NORMAL



class RadauError(Exception):
    def __init__(self,expr):
        self.expr = expr
    def __str__(self):
        return self.expr

class RKS3CBase(ABC,Explicit_ODE): #Runge-Kutta S=3 Complex eigen pair Base class

    rtol=1.e-6 #realative tolerance
    atol=1.e-6 #absolute tolerance
    tol =1.e-6 #tollerance for newton iteration
    kappa = 1e-1 #The kappa value used for newton stopping in eq (15)
    maxit=50 #maximum number of newton iterations
    maxsteps=10000000 #maximum number of steps
    r=1 #r is the ratio between the previouse step size and the current

    #used for stoping after single newton itt
    Uround = sys.float_info.min

    #the bounds to decide weather or not to adjust the step size eq (41)
    c1 = 0.95
    c2 = 1.2


    def __init__(self,problem, krylov=True, ilu=False):
        """
        Initiates the solver.

            Parameters::

                problem
                            - The problem to be solved. Should be an instance
                              of the 'Explicit_Problem' class.
        """
        Explicit_ODE.__init__(self, problem) #Calls the base class

        #Solver options
        try:
            self.options['h'] = problem.h
            self.h = problem.h
        except:
            self.options['h'] = 1e-4
            self.h = 1e-4
        self._sparseJ = True
        self._dynamic_step = False#True

        #Statistics
        self.statistics["nsteps"] = 0
        self.statistics["nfcns"] = 0
        self.num_jac_evals = 0
        self.num_lu_decomps = 0

        #method parameter:
        self.y_dim = len(self.problem.y0)
        self.RTol = np.array([self.rtol]*self.y_dim)
        self.ATol = np.array([self.atol]*self.y_dim)
        self._3ATol = np.hstack((self.ATol,self.ATol,self.ATol))
        self._3RTol = np.hstack((self.RTol,self.RTol,self.RTol))

        #internal flags
        self._first = True
        self._recomp_jac = True
        self._recomp_LHS = True
        self._newt_fail_flag = False

        #internal values
        self.J = None
        self._lu = None
        self._krylov = krylov
        self._ilu = ilu
        self._verbose = 1

    # solve newton step
    def _solve(self, LHS, RHS, M = None ):
        # newton_krylov
        if self._krylov:
            n = RHS.shape[0]//3 #get length of columns for each stage
            dwk = np.zeros_like(RHS)
            rtol = 1e-2
            atol = self.tol * rtol

            iteration = 0
            residual = 0.
            def callb(*args, **kwargs):
                nonlocal iteration
                nonlocal residual
                iteration += 1
                residual = args[0]
                #print(f"iteration {iteration}: {residual}")

            iluM = None
            if M is not None:
                iluM = LinearOperator(LHS[0].shape, M[0].solve)
            dwk[:n], info = gmres(LHS[0], RHS[:n], M=iluM, tol=rtol, atol=atol, callback=callb, callback_type='pr_norm' )  #this corresponds to the \gamma block
            if info == 0:
                print(f"GMRes converged: its = {iteration}, residual = {residual}\n")
            else:
                print("GMRes diverged!\n")

            # reset iteration count
            iteration = 0
            residual = 0.
            if M is not None:
                iluM = LinearOperator(LHS[1].shape, M[1].solve)
            dwk[n:], info = gmres(LHS[1], RHS[n:], M=iluM, tol=rtol, atol=atol, callback=callb, callback_type='pr_norm' )  #this corresponds to the \alpha and \beta block

            if self._verbose and info == 0:
                print(f"GMRes converged: its = {iteration}, residual = {residual}\n")
            else:
                print("GMRes diverged!\n")
            return dwk

        else: # LU-decomposition
            assert self._LUSolve is not None
            return self._LUSolve(self._lu,RHS) #eq (17a)

    # compute ilu
    def _computeILU(self, LHS ):
        if self._ilu:
            fillFactor = 50
            M = []
            for mat in LHS:
                M.append(spilu( mat, drop_tol=1e-6, fill_factor=fillFactor ))
            return M
        else:
            return None

    #set and get for sparse Jacobian, need to redefine the lu decompose and
    #lu solve methods here
    def _set_sparse(self,sp):
        self.options["sparse"] = bool(sp)
        if sp:
            def lu_decomp_s(LHS):
                lu1 = splu(LHS[0])
                lu2 = splu(LHS[1])
                return (lu1,lu2)
            self._LUDecomp = lu_decomp_s

            def lu_solve_s(lu,RHS):
                n = RHS.shape[0]//3 #get length of columns for each stage
                dwk = np.zeros_like(RHS)
                dwk[:n] = lu[0].solve(RHS[:n]) #this corresponds to the \gamma block
                dwk[n:] = lu[1].solve(RHS[n:])  #this corresponds to the \alpha and \beta block
                return dwk
            self._LUSolve = lu_solve_s
        else:
            def lu_decomp_d(LHS):
                lu1 = lu_factor(LHS[0])
                lu2 = lu_factor(LHS[1])
                return (lu1,lu2)
            self._LUDecomp = lu_decomp_d

            def lu_solve_d(lu,RHS):
                n = RHS.shape[0]//3
                dwk = np.zeros_like(RHS)
                dwk[:n] = lu_solve(lu[0],RHS[:n])
                dwk[n:] = lu_solve(lu[1],RHS[n:])
                return dwk
            self._LUSolve = lu_solve_d

    def _get_sparse(self):
        return self.options["sparse"]
    _sparseJ = property(_get_sparse,_set_sparse)

    #set and get for h parameter, when h changes we also want to recompute the
    #LHS
    def _set_h(self,h):
        self.options["h"] = float(h)
    def _get_h(self):
        return self.options["h"]
    h=property(_get_h,_set_h)


    def integrate(self,t,y,tf,opts):
        """
        _integrates (t,y) values until t > tf
        """
        self.h = min(self.h,abs(tf-t))

        tres = []
        yres = []

        z0 = np.zeros((self.y_dim*3))

        #lists for storing results
        for i in range(self.maxsteps):
            if t>= tf:
                break

            t,y = self._step(t,y,z0)

            self._first = False

            self.t = t #used for Radau error approximation

            #store the results in array to be returned when done
            tres.append(t)
            yres.append(y.copy())


            if self.h > np.abs(tf-t):
                h_old = self.h / self.r
                self.h = np.abs(tf-t)
                self.r = self.h/h_old
                self._recomp_LHS=True

            if abs(self.r-0.5)<0.1:
                #if we are to close to the explosion point, change value
                h_old = self.h/self.r
                self.r = 0.35
                self.h = self.r * h_old
                self._recomp_LHS=True

            if abs(self.h) < self.Uround:
                break

        else:
            raise RadauError(f"final time not reached within maximum number of steps ({self.maxsteps})")
        return ID_PY_OK, tres, yres


    def _step(self,t,y,z0):
        #update statistics
        self.statistics["nsteps"] += 1

        #recomp jacobian if the flag is set
        if self._recomp_jac:
            self.J = self._jacobian(t,y)
            self._recomp_jac = False
            self._recomp_LHS = True

        #newton itteration
        y,h, total1, total2, total3 = self._newton(self.J,y.copy(),t,z0)

        #!!! here printing stuff in case all stage values need to be available
        '''
        f = open('results_{}.txt'.format(h),'a')  # w : writing mode  /  r : reading mode  /  a  :  appending mode
        f.write('{}\n'.format(total1))
        f.write('{}\n'.format(total2))
        f.write('{}\n'.format(total3))
        f.close()
        '''

        return t + h, y

    def _assemble( self, J ):
        LHS, M = None, None
        #setup LHS and lu decompose if needed
        if self._recomp_LHS or self._lu == None:
            LHS = self._compLHS(J)
            if self._krylov:
                M = self._computeILU( LHS )
            else:
                self._lu = self._LUDecomp(LHS)
                self.num_lu_decomps += 1
                self._recomp_LHS = False
        return LHS, M

    def _newton(self,J,y0,t,z0=None):

        #handy dimension variable
        n  = self.y_dim

        #setup newton starting value
        zk = z0 if type(z0) is np.ndarray else np.zeros(3*n) #n*s zeros/values of z0
        wk = self._zTOw(zk) #vector of zeros/values of z0 multiplied by T

        #newton stopping variables
        ndzk = None #Norm of delta Zk
        theta_k = None #Theta_k

        LHS, M = self._assemble( J )

        for i in range(1,self.maxit):
            #compute the RHS
            RHS = self._compRHS(wk,zk,y0)

            #compute the delta W_k
            # dwk = self._LUSolve(self._lu,RHS) #eq (17a)
            # LHS = self._compLHS(J)
            dwk = self._solve( LHS, RHS, M ) #eq (17a)

            #convert delta W_ktodelta Z_k
            dzk = self._wTOz(dwk)

            #increment wk and zk with the corresponding delta values
            wk += dwk #eq (17b)
            zk += dzk

            #---------newton stopping condition-----------

            #for sci we require the max of z_k and z_{k-1} termwise
            diff = dzk.copy()
            diff[diff>0] = 0 #wherever the value has increased we want to keep
            sci = self._3ATol + (zk-diff)*self._3RTol

            #norm of delta zk
            ndzk = self._norm(dzk,sci)

            #declare the eta_k variable so it is outside the if scope
            eta_k = None
            if i == 1 :#first step in newton
                if '_eta' in self.__dict__:
                #one step stopping
                    eta_k = max(self._eta,self.Uround)**.8
                else:
                #first time step, needs at least 2 newton
                    ndzkm1 = ndzk
                    continue
            else:
                #compute theta_k and eta_k
                theta_k = ndzk/ndzkm1 # eq (14)
                eta_k   = theta_k/(1-theta_k) #eq (15)

                #check if we are diverging
                if theta_k > 1:

                    if not self._dynamic_step:
                        raise RadauError(f'newton iteration diverged')

                    #adjust step size
                    self.h /= 2
                    self.r /= 2

                    #if r is to close to 0.5 adjust it down further,
                    #this is due to explosion of values in error estimation
                    #TODO: move this into the set h function.
                    if (self.r-0.5)<0.1:
                        self.h *= 0.35 /self.r
                        self.r = 0.35

                    #recompute the jacobian
                    self.J = self._jacobian(t,y0)
                    self._recomp_LHS = True

                    #re-run newton
                    return self._newton(self.J,y0,t,z0)

            #test if newton has converged, eq (14)
            if eta_k * ndzk <= self.kappa * self.tol:
                #save old eta for singe step stopping
                self._eta = eta_k

                #new y value
                y1 = self._zTOy(y0,zk)
                #the h value that was used
                h = self.h

                #------determine approximate error---------
                if self._dynamic_step:
                    #approximate error magnitude
                    err_mag = self._normed_aprox_err(zk,y0,y1,n)

                    #if error magnitude > 1 last h will not be changed since
                    #we will rerun this step.
                    h_old = self.h/ ( self.r if err_mag > 1 else 1)

                    #compute new step size
                    h_new = self._adjust_h(err_mag,step_accept = err_mag < 1, itt = i)
                    #test if we should adjust step eq (41):
                    if not (self.c1*self.h <= h_new and h_new <= self.c2 * self.h):
                        #set flag to recompute the LHS in the next itteration
                        self._recomp_LHS = True
                        #calculate the new ratio
                        r = h_new/h_old

                        #special condition for the Lobatto to avoid coefficient
                        #explosion when computeing the error magnitude
                        if abs(r-0.5)<0.1:
                            #if we are to close to the explosion point, change value
                            r = 0.35
                            h_new = r * h_old
                        #save adjusted values
                        self.r = r
                        self.h = h_new
                    else:
                        #if we dont adjust step size
                        h_new = self.h
                        self.r = 1.0


                    if  err_mag > 1:
                        #approximate error is to large, restart with smaller step size
                        return self._newton(J,y0,t,z0)

                    #store z^(k-1) for error estimation in the next step.
                    self.zkm1 = zk.copy()

                #return result and the h used.
                if self._verbose:
                    print("Newton converged")
                return y1, h, self._znewy1(y0,zk)[0],y1[0],self._znewy2(y0,zk)[0]

            #update the norm for newton stopping condition
            ndzkm1 = ndzk

        else:
            #if we do not converge within number of newton itteration steps
            if not self._newt_fail_flag:
                print("newton fail")
                self._newt_fail_flag = True
                #rerun with new Jacobian if not done yet
                self.J = self._jacobian(t,y0)
                self._recomp_LHS = True
                #re-run newton
                return self._newton(self.J,y0,t,z0)
            #otherwise raise
            raise RadauError(f'newton iteration did not converge within {self.maxit} iterations')

    @abstractclassmethod
    def _normed_aprox_err(self,zk,y0,y1,n):
        #abstaract method that each version of this code must impliment seperately
        return np.Inf

    @abstractclassmethod
    def _adjust_h(self,err_mag,TOL = 1e-5,step_accept = True,itt = 0):
        return np.Inf

    def _norm(self,vec,sc):
        #calculates the norm of a vector according to eq (38)
        return np.sqrt(
                    (1/vec.shape[0])*
                    np.sum(
                        (vec/sc)**2
                    )
                )



    def _compRHS(self,wk,zk,y):
        #computes the RHS of eq (17a)

        #dimenson of system
        n = self.y_dim
        #print(n)
        #initial term:
        term1 = -np.array(n*[self.gamma/self.h] + \
                        2*n*[(self.alpha)/self.h]) \
                    * wk

        term1[n:2*n] += self.betta/self.h * wk[2*n: ]
        term1[2*n: ] -= self.betta/self.h * wk[n:2*n]

        #function term
        fcn = self.problem.rhs
        f = np.array([fcn(self.t+self.c[0]*self.h,y + zk[:n]),
            fcn(self.t+self.c[1]*self.h,y + zk[n:2*n]),
            fcn(self.t+self.c[2]*self.h,y + zk[2*n:])])
        prod = (self.TI @ f).flatten()

        self.statistics["nfcns"] += 3

        return term1 + prod

    def _compLHS(self,J):
        #construction of the LHS of eq (17a)

        spmat1 = csc_matrix(J)
        spmat1 *= -1.0
        diag = spmat1.diagonal()
        diag[:] += self.gamma/self.h
        spmat1.setdiag( diag )

        A  = csc_matrix(J) #
        A *= -1.0 # we need self.alpha/self.h - J
        diag = A.diagonal()
        diag[:] += self.alpha/self.h
        A.setdiag( diag )
        diag[:] = self.betta/self.h
        B1 = diagonal( -diag )
        B2 = diagonal(  diag )
        spmat2 = bmat( [[A, B1], [B2, A]], format="csc" )

        return (spmat1,spmat2)


    def _jacobian(self,t,y):
        """
        calculates the jacobian
        """
        if self.problem_info['jac_fcn']:
            self.num_jac_evals += 1
            J = self.problem.jac(t,y)
            self._recomp_jac = False
            return J
        else:
            raise RadauError('The current implimentation cannot calculate an approximate jacobian yet,' +
                    'hence a jacobian function must be given to the problem')

    def _zTOw(self,z):
        #TODO: this might one returns return directly...
        #convert from z to w
        w = (self.TI@ z.reshape((3,-1))).flatten()
        return w

    def _wTOz(self,w):
        #convert from w to z
        z = (self.T @ w.reshape((3,-1))).flatten()
        return z

    def _zTOy(self,y,z):
        #convert from z to y
        y = y + self.d @ z.reshape((3,-1))
        return y

    def _znewy1(self,y,z):
        #convert from z to y
        y = y + np.array([1,0,0]) @ z.reshape((3,-1))  ### !!!
        return y

    def _znewy2(self,y,z):
        #convert from z to y
        y = y + np.array([0,1,0]) @ z.reshape((3,-1))  ### !!!
        return y

    def print_statistics(self, verbose=NORMAL):
        """
        Prints the run-time statistics for the problem.
        """
        Explicit_ODE.print_statistics(self, verbose) #Calls the base class
        self.log_message('\nSolver options:\n',                                              verbose)
        self.log_message(' Solver                  : 3-stage Lobatto IIIC',                  verbose)
        self.log_message(' Number of J evaluations : ' + str(self.num_jac_evals),    verbose)
        self.log_message(' Number of LU decompos   : ' + str(self.num_lu_decomps),   verbose)
        self.log_message('\n\n\n',verbose)


class Radau5ODE(RKS3CBase):
    #method constants:

    #T - matrix
    T = np.array([
        [9.1232394870892942792e-2,-0.14125529502095420843,-3.0029194105147424492e-02],
        [0.24171793270710701896e0,0.20412935229379993199e0,0.38294211275726193779e0],
        [0.96604818261509293619e0,1.0,0.0]
        ])

    #T inverse - matrix
    TI= np.array([
        [4.3255798900631553510e0,0.33919925181580986954e0,0.54177053993587487119e0  ],
        [-4.1787185915519047273e0,-0.32768282076106238708e0,0.47662355450055045196e0],
        [-0.50287263494578687595e0,2.5719269498556054292e0,-0.59603920482822492497e0]
        ])

    #eigen values
    gamma : float = 3.63783425
    alpha : float = 2.68108287
    betta : float = 3.0504302

    SQ6 = np.sqrt(6)

    #ignore errors here, this is something is wrong in the error detection this
    #works...
    c = np.array([
        (4.0-SQ6)/10.0,
        (4.0+SQ6)/10.0,
        1
        ])

    #d vector used in converting z to y
    d = np.array([0,0,1])
    #e vector used in error approximation
    e = np.array([-13-7*SQ6,-13+7*SQ6,-1]) /(3*gamma)

    #since d is (0,0,1) we redefine the z to y conversion
    def _zTOy(self, y, z):
        return y + z[2*self.y_dim:]

    def _normed_aprox_err(self,zk,y0,y1,n):
        zpart = self.e[0] * zk[:n] + self.e[1] * zk[n:2*n] + self.e[2] * zk[2*n:]
        ydiff = self.h*self.problem.rhs(self.t,y0)/self.gamma + zpart

        #invert LHS1 matrix
        #TODO: it might be faster to generate the identity matrix and just use the n'th column instead of regenerateign... maybe store in self?
        I = np.eye(n)
        inv = None
        if self._sparseJ:
            inv = self._lu[0].solve(I)
        else:
            inv = lu_solve(self._lu[0],I)
        inv *= self.gamma/self.h
        err   = inv @ ydiff
        sci = self.ATol + np.maximum(np.abs(y0),np.abs(y1))*self.RTol
        mag = self._norm(err,sci)
        if mag > 1:
            mag = self._norm(inv @ self.h * self.problem.rhs(self.t,y0)/self.gamma + zpart,sci)
        return mag

    def _adjust_h(self,err_mag,TOL = 1e-5,step_accept = True,itt = 0):
        fac = 0.9 * (2*self.maxit+1)/(2*self.maxit+itt)
        return fac * self.h * err_mag ** (-1/4)

class Lobatto3ODE(RKS3CBase):

    #-------- Method constants -----------
    # T matrix
    T = np.array([
        [0.4072639531732107,-0.44308062047843144,0.31680119776208315],
        [0.18547209365357897,0.1305271017756723,-0.38187534481524843],
        [0.8942796961362183,0.735153359223255,0.0]
        ])
    # T inverse matrix
    TI= np.array([
        [1.0326372242409414,0.8566688421623923,0.47027336073401904],
        [-1.2561549117980622,-1.0420976007887581,0.7881948366175593],
        [0.07217833861373389,-2.558776912784013,0.49781525580043673],
        ])

    #eigen values gamma, alpha and betta
    gamma = 2.6258168189584676
    alpha = 1.6870915905207662
    betta = -2.508731754924879

    # c,d and b (see paper for reference)
    c = np.array([0.,1/2.,1.])
    d = np.array([0,0,1])
    b = np.array([1/6,2/3,1/6])

    #e used for one step error approximation
    e = np.array([-4.0,0,-1]) #this is e A^-1 since we need to do this conversion...

    #special flag used when wanting to compair error estimates
    _do_comp_err = False #set this to true to compair the error

    #free parameter a used in error estimation
    a = 5

    def __init__(self,problem):
        RKS3CBase.__init__(self,problem)

        #list for storeing error compairison if flag is set
        self.err_comp = []

    #overwrite z to y conversion to be more efficient
    def _zToy(self,y,z):
        return y + z[2*self.y_dim:]


    #Method to calculate the coefficients for the error estimation
    def coef(self,r):
        if abs(r-0.5)<0.09:
            raise RadauError(f'Some internal error; the relative distance in step size is to close to 0.5, r:{r}')

        #TODO: Optimize this method
        #-devisor is always the same
        #-delta3 = -delta1 -delta2
        #-precalculate r^3 and r^2 (this might make it more efficient?)
        beta1 = (12*r**3 + 14*r**2 + 21*r + 9)/(4*r**3 + 4*r**2 + 3*r - 3)
        beta2 = (16*r**3 + 8*r**2 - 12*r - 12)/(4*r**3 + 4*r**2 + 3*r - 3)
        beta3 = 0
        delta1=(12*r**3 + 9*r**2 + 3*r)/(4*r**3 + 4*r**2 + 3*r - 3)
        delta2=(8*r**4 - 24*r**3 - 36*r**2 - 12*r)/(4*r**3 + 4*r**2 + 3*r - 3)
        delta3=(-8*r**4 + 12*r**3 + 27*r**2 + 9*r)/(4*r**3 + 4*r**2 + 3*r - 3)

        return (beta1,beta2,delta1,delta2,delta3)


    def _normed_aprox_err(self,zk,y0,y1,n):
        #calculate scaleing of error in the norm from hairer
        sc = self.ATol + np.maximum(np.abs(y0),np.abs(y1))*self.RTol

        #if we are in the first step
        if self._first or self._do_comp_err:
            #do one step error estimation
            zpart = self.e[0] * zk[:n] + self.e[1] * zk[n:2*n] + self.e[2] * zk[2*n:]
            ydiff = self.h*self.problem.rhs(self.t,y0)/self.gamma + zpart
            self.statistics["nfcns"] += 1

            #invert LHS1 matrix
            #TODO: it might be faster to store the identity matrix instead of generateing it each time?
            """
            #Filtering, To be added later,
            #since this should only be called once there is no need to
            #optimize the inverse further and store it to be reused...
            I = np.eye(n)
            if self._sparseJ:
                inv = self._lu[0].solve(I)
            else:
                inv = lu_solve(self._lu[0],I)
            inv *= self.gamma/self.h
            """
            #normalize error vector
            mag = self._norm(ydiff,sc)

            #if we want to compair the error to the two step error
            if (not self._first):
                beta1,beta2,delta1,delta2,delta3 = self.coef(self.r)
                err_vec_tmp = self.a * (zk[2*n:] - (delta1*self.zkm1[:n] \
                    + delta2*self.zkm1[n:2*n] + delta3*self.zkm1[2*n:] \
                    + beta1 * zk[:n] + beta2 * zk[n:2*n]))
                err = self._norm(err_vec_tmp,sc)
                err_old = mag
                #return err

                self.err_comp.append((err ,mag,self.t,self.h))
                if mag > 1:
                    self.statistics["nfcns"] += 1
                    mag = self._norm(self.h * self.problem.rhs(self.t,y0)/self.gamma + zpart,sc)


            else:
                err = mag
                #if the step fails do more accurate error estimation
                if err > 1:
                    self.statistics["nfcns"] += 1
                    err = self._norm(self.h * self.problem.rhs(self.t,y0)/self.gamma + zpart,sc)
            return err

        else:
            #do two step error estimation
            #calculate coefficient
            beta1,beta2,delta1,delta2,delta3 = self.coef(self.r)
            #determine error vector
            err_vec = self.a * (zk[2*n:] - (delta1*self.zkm1[:n] \
                    + delta2*self.zkm1[n:2*n] + delta3*self.zkm1[2*n:] \
                    + beta1 * zk[:n] + beta2 * zk[n:2*n]))
            #normalize error
            err = self._norm(err_vec,sc)
            return err

    def _adjust_h(self,err_mag,TOL = 1e-5,step_accept = True,itt = 0):
        #function to adjust the step size
        fac = 0.9 * (2*self.maxit+1)/(2*self.maxit+itt)
        return fac * self.h * err_mag ** (-1/4)



if __name__ == '__main__':
    #-----example usage-----
    from assimulo.ode import Explicit_Problem
    import matplotlib.pyplot as plt

    #define the problem
    g : float = 9.81
    l : float = 0.5
    theta : float = np.pi/2.
    def rhs(t,y):
        return np.array([
            y[1],
            np.sin(y[0])*g/l
            ])

    def J(t,y):
        return np.array([
            [0, 1],
            [np.cos(y[0])*g/l,0]
            ])

    #--helper functions for calculating period
    def arith_geo_mean(a,b):
        while True:
            a,b = (a+b)/2, np.sqrt(a*b)
            yield a,b

    from itertools import islice
    def elliptic_integral(k,tol=1e-5,maxiter=100):
        a_0,b_0 = 1.,np.sqrt(1-k**2)
        for a,b in islice(arith_geo_mean(a_0,b_0),maxiter):
            if abs(a-b) < tol:
                return np.pi /(2*a)
        else:
            raise Exception('Algorithm did not converge')

    periode = 4*np.sqrt(l/g) * elliptic_integral(np.sin(theta/2),tol=1e-10)

    #----Initialize Problem
    pend_model = Explicit_Problem(rhs,y0=np.array([theta,0.]))
    pend_model.name = 'Pendulum simulation'
    pend_model.jac = J
    pend_model.h = 0.01

    #----Initialize Solver
    sim = Lobatto3ODE(pend_model)
    sim.h = 0.016
    sim._do_comp_err = False
    sim._dynamic_step = False
    sim.maxit=10
    #----Simulate the solver
    t,y = sim.simulate(periode)
    #----Plot results
    sim.plot()
    plt.show()

