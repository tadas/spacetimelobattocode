#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  6 08:08:25 2021

@author: leaversbach
"""


import sys #for min float number
import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as alg
from scipy.sparse.linalg import splu
from scipy.sparse import bmat
from scipy.sparse import diags as diagonal
from scipy.sparse import csc_matrix
from abc import ABC, abstractclassmethod
from scipy.linalg import lu_factor,lu_solve

from assimulo.explicit_ode import Explicit_ODE
from assimulo.ode import ID_PY_OK,NORMAL



class RadauError(Exception):
    def __init__(self,expr):
        self.expr = expr
    def __str__(self):
        return self.expr


class RKS2CBase(ABC,Explicit_ODE): #Runge-Kutta s=2 complex eigen pair base class

    rtol=1.e-6 #realative tolerance
    atol=1.e-6 #absolute tolerance
    tol =1.e-6 #tolerance for newton iteration
    kappa = 1e-1 #The kappa value used for newton stopping in eq (15)
    maxit= 50 #maximum number of newton itterations
    maxsteps=10000 #maximum number of steps
    r=1 #r is the ratio between the previouse step size and the current

    #used for stopping after single newton iteration
    Uround = sys.float_info.min

    #the bounds to decide weather or not to adjust the step size eq (41)
    c1 = 0.95
    c2 = 1.2


    def __init__(self,problem):
        """
        Initiates the solver.

            Parameters::

                problem
                            - The problem to be solved. Should be an instance
                              of the 'Explicit_Problem' class.
        """
        Explicit_ODE.__init__(self, problem) #Calls the base class

        #Solver options
        try:
            self.options['h'] = problem.h
            self.h = problem.h
        except:
            self.options['h'] = 0.01#1e-4
            self.h = 0.01#1e-4
        self._sparseJ = True
        self._dynamic_step = False

        #Statistics
        self.statistics["nsteps"] = 0
        self.statistics["nfcns"] = 0
        self.num_jac_evals = 0
        self.num_lu_decomps = 0

        #method parameter:
        self.y_dim = len(self.problem.y0)
        self.RTol = np.array([self.rtol]*self.y_dim)
        self.ATol = np.array([self.atol]*self.y_dim)
        self._3ATol = np.hstack((self.ATol,self.ATol))
        self._3RTol = np.hstack((self.RTol,self.RTol))

        #internal flags
        self._first = True
        self._recomp_jac = True
        self._recomp_LHS = True
        self._newt_fail_flag = False

        #internal values
        self.J = None
        self._lu = None


    #set and get for sparse Jacobian, need to redefine the lu decompose and
    #lu solve methods here
    def _set_sparse(self,sp):
        self.options["sparse"] = bool(sp)
        if sp:
            def lu_decomp_s(LHS):
                lu1 = splu(LHS)
                return lu1
            self._LUDecomp = lu_decomp_s

            def lu_solve_s(lu,RHS):
                dwk = lu.solve(RHS)
                return dwk
            self._LUSolve = lu_solve_s
        else:
            def lu_decomp_d(LHS):
                lu1 = lu_factor(LHS)
                return lu1
            self._LUDecomp = lu_decomp_d

            def lu_solve_d(lu,RHS):
                return lu_solve(lu,RHS)
            self._LUSolve = lu_solve_d

    def _get_sparse(self):
        return self.options["sparse"]

    _sparseJ = property(_get_sparse,_set_sparse)

    #set and get for h parameter, when h changes we also want to recompute the
    #LHS
    def _set_h(self,h):
        self.options["h"] = float(h)
    def _get_h(self):
        return self.options["h"]
    h=property(_get_h,_set_h)


    def integrate(self,t,y,tf,opts):
        """
        _integrates (t,y) values until t > tf
        """
        self.h = min(self.h,abs(tf-t))

        tres = []
        yres = []

        z0 = np.zeros((self.y_dim*2))

        #lists for storing results
        for i in range(self.maxsteps):
            if t>= tf:
                break

            t,y = self._step(t,y,z0)

            self._first = False

            self.t = t #used for Radau error approximation

            #store the results in array to be returned when done
            tres.append(t)
            yres.append(y.copy())


            if self.h > np.abs(tf-t):
                h_old = self.h / self.r
                self.h = np.abs(tf-t)
                self.r = self.h/h_old
                self._recomp_LHS=True

            if abs(self.r-0.5)<0.1:
                #if we are to close to the explosion point, change value
                h_old = self.h/self.r
                self.r = 0.35
                self.h = self.r * h_old
                self._recomp_LHS=True

            if abs(self.h) < self.Uround:
                break

        else:
            raise RadauError('final time not reached within maximum number of steps ({self.maxsteps})')
        return ID_PY_OK, tres, yres


    def _step(self,t,y,z0):
        #update statistics
        self.statistics["nsteps"] += 1
        #recompute jacobian if the flag is set
        if self._recomp_jac:
            self.J = self._jacobian(t,y)
            self._recomp_jac = False
            self._recomp_LHS = True

        #newton iteration
        y,h, total1, total2 = self._newton(self.J,y.copy(),t,z0)
        
        #!!! here printing stuff in case all stage values need to be available
        '''
        f = open('results_{}.txt'.format(h),'a')  # w : writing mode  /  r : reading mode  /  a  :  appending mode
        f.write('{}\n'.format(total1))
        f.write('{}\n'.format(total2))
        f.close()
        '''
        
        return t+h,y

    def _newton(self,J,y0,t,z0=None):

        #handy dimension variable
        n  = self.y_dim

        #setup newton starting value
        zk = z0 if type(z0) is np.ndarray else np.zeros(3*n)
        wk = self._zTOw(zk)

        #newton stopping variables
        ndzk = None #Norm of delta Zk
        theta_k = None #Theta_k

        #setup LHS and lu decompose if needed
        if self._recomp_LHS or self._lu == None:
            LHS = self._compLHS(J)
            self._lu = self._LUDecomp(LHS)
            self.num_lu_decomps += 1
            self._recomp_LHS = False

        for i in range(1,self.maxit):
            #compute the RHS
            RHS = self._compRHS(wk,zk,y0)

            #compute the delta W_k
            dwk = self._LUSolve(self._lu,RHS) #eq (17a)  #TODO: replace by GMRES?

            #convert delta W_ktodelta Z_k
            dzk = self._wTOz(dwk)

            #increment wk and zk with the corresponding delta values
            wk += dwk #eq (17b)
            zk += dzk

            #---------newton stopping condition-----------

            #for sci we require the max of z_k and z_{k-1} termwise
            diff = dzk.copy()
            diff[diff>0] = 0 #wherever the value has increased we want to keep
            sci = self._3ATol + (zk-diff)*self._3RTol

            #norm of delta zk
            ndzk = self._norm(dzk,sci)

            #declare the eta_k variable so it is outside the if scope
            eta_k = None
            if i == 1 :#first step in newton
                if '_eta' in self.__dict__:
                #one step stopping
                    eta_k = max(self._eta,self.Uround)**.8
                else:
                #first time step, needs at least 2 newton
                    ndzkm1 = ndzk
                    continue
            else:
                #compute theta_k and eta_k
                theta_k = ndzk/ndzkm1 # eq (14)
                eta_k   = theta_k/(1-theta_k) #eq (15)
                #print('theta_k:',theta_k)

                #check if we are diverging
                if theta_k > 1:

                    if not self._dynamic_step:
                        raise RadauError('newton iteration diverged')

                    #adjust step size
                    self.h /= 2
                    self.r /= 2

                    #if r is to close to 0.5 adjust it down further,
                    #this is due to explosion of values in error estimation
                    #TODO: move this into the set h function.
                    if (self.r-0.5)<0.1:
                        self.h *= 0.35 /self.r
                        self.r = 0.35

                    #recompute the jacobian
                    self.J = self._jacobian(t,y0)
                    self._recomp_LHS = True

                    #re-run newton
                    return self._newton(self.J,y0,t,z0)

            #test if newton has converged, eq (14)
            if eta_k * ndzk <= self.kappa * self.tol:
                #save old eta for singe step stopping
                self._eta = eta_k

                #new y value
                y1 = self._zTOy(y0,zk)
                #the h value that was used
                h = self.h

                #------determine approximate error---------
                if self._dynamic_step:
                    #approximate error magnitude
                    err_mag = self._normed_aprox_err(zk,y0,y1,n)

                    #if error magnitude > 1 last h will not be changed since
                    #we will rerun this step.
                    h_old = self.h/ ( self.r if err_mag > 1 else 1)

                    #compute new step size
                    h_new = self._adjust_h(err_mag,step_accept = err_mag < 1, itt = i)
                    #test if we should adjust step eq (41):
                    if not (self.c1*self.h <= h_new and h_new <= self.c2 * self.h):
                        #set flag to recompute the LHS in the next itteration
                        self._recomp_LHS = True
                        #calculate the new ratio
                        r = h_new/h_old

                        #special condition for the Lobatto to avoid coefficient
                        #explosion when computeing the error magnitude
                        if abs(r-0.5)<0.1:
                            #if we are to close to the explosion point, change value
                            r = 0.35
                            h_new = r * h_old
                        #save adjusted values
                        self.r = r
                        self.h = h_new
                    else:
                        #if we dont adjust step size
                        h_new = self.h
                        self.r = 1.0


                    if  err_mag > 1:
                        #approximate error is to large, restart with smaller step size
                        return self._newton(J,y0,t,z0)

                    #store z^(k-1) for error estimation in the next step.
                    self.zkm1 = zk.copy()

                #return result and the h used.
                return y1, h, self._znewy(y0,zk)[0],y1[0]

            #update the norm for newton stopping condition
            ndzkm1 = ndzk

        else:
            #if we do not converge within number of newton itteration steps
            if not self._newt_fail_flag:
                print("newton fail")
                self._newt_fail_flag = True
                #rerun with new Jacobian if not done yet
                self.J = self._jacobian(t,y0)
                self._recomp_LHS = True
                #re-run newton
                return self._newton(self.J,y0,t,z0)
            #otherwise raise
            raise RadauError('newton iteration did not converge within {self.maxit} iterations')

    @abstractclassmethod
    def _adjust_h(self,err_mag,TOL = 1e-5,step_accept = True,itt = 0):
        return np.Inf

    def _norm(self,vec,sc):
        #calculates the norm of a vector according to eq (38)
        return np.sqrt((1/vec.shape[0])*np.sum((vec/sc)**2))

    def _compRHS(self,wk,zk,y):
 
        #dimenson of system
        n = self.y_dim
        #initial term:
        term1 = -np.array(2*n*[(self.alpha)/self.h]) * wk #element-wise multiplication of the arrays -> array
        term1[:n] += self.beta/self.h * wk[n:]
        term1[n:] -= self.beta/self.h * wk[:n]

        #function term
        fcn = self.problem.rhs
        f = np.array([fcn(self.t+self.c[0]*self.h,y + zk[:n]),
            fcn(self.t+self.c[1]*self.h,y + zk[n:])])
        prod = (self.TI @ f).flatten()

        self.statistics["nfcns"] += 2

        return term1 + prod

    def _compLHS(self,J):
        #construction of the LHS of eq (17a)

        #dimension of system
        n = self.y_dim

        #construct of blocks according to eq (20)

        # diag 1
        A  = csc_matrix(J) 
        A *= -1.0 # we need self.alpha/self.h - J
        diag = A.diagonal()
        diag[:] += self.alpha/self.h
        A.setdiag( diag )
        diag[:] = self.beta/self.h
        B1 = diagonal( -diag )
        B2 = diagonal(  diag )
        spmat = bmat( [[A, B1], [B2, A]], format="csc" )
 
        return spmat

    def _jacobian(self,t,y):
        """
        calculates the jacobian
        """
        if self.problem_info['jac_fcn']:
            self.num_jac_evals += 1
            J = self.problem.jac(t,y)
            self._recomp_jac = False
            return J
        else:
            raise RadauError('The current implementation cannot calculate an approximate jacobian yet,' +
                    'hence a jacobian function must be given to the problem')

    def _zTOw(self,z):
        #TODO: this might one returns return directly...
        #convert from z to w
        w = (self.TI@ z.reshape((2,-1))).flatten()  ### !!!
        return w

    def _wTOz(self,w):
        #convert from w to z
        z = (self.T @ w.reshape((2,-1))).flatten()  ### !!!
        return z

    def _zTOy(self,y,z):
        #convert from z to y
        y = y + self.d @ z.reshape((2,-1))  ### !!!
        return y
    
    def _znewy(self,y,z):
        #convert from z to y
        y = y + np.array([1,0]) @ z.reshape((2,-1))  ### !!!
        return y

    def print_statistics(self, verbose=NORMAL):
        """
        Prints the run-time statistics for the problem.
        """
        Explicit_ODE.print_statistics(self, verbose) #Calls the base class
        self.log_message('\nSolver options:\n',                                              verbose)
        self.log_message(' Solver                  : 2-stage Lobatto IIIC',                  verbose)
        self.log_message(' Number of J evaluations : ' + str(self.num_jac_evals),    verbose)
        self.log_message(' Number of LU decompos   : ' + str(self.num_lu_decomps),   verbose)
        self.log_message('\n\n\n',verbose)


class Lobatto2ODE(RKS2CBase):

    #-------- Method constants -----------
    # T matrix
    T = np.array([
        [0.70710678,0.],
        [0.,0.70710678]
        ])
    # T inverse matrix
    TI= np.array([
        [1.41421356,0.],
        [0.,1.41421356]
        ])

    #eigen values gamma, alpha and betta
    alpha = 1.
    beta = -1.

    # c,d and b (see paper for reference)
    b = np.array([1/2,1/2])
    c = np.array([0.,1.])
    d = np.array([0,1])

    #special flag used when wanting to compair error estimates
    _do_comp_err = False #set this to true to compair the error

    #free parameter a used in error estimation
    a = 5

    def __init__(self,problem):
        RKS2CBase.__init__(self,problem)

        #list for storeing error compairison if flag is set
        self.err_comp = []

    #overwrite z to y conversion to be more efficient
    def _zToy(self,y,z):
        return y + z[2*self.y_dim:]

    '''
    #Method to calculate the coefficients for the error estimation
    def coef(self,r):
        if abs(r-0.5)<0.09:
            raise RadauError(f'Some internal error; the relative distance in step size is to close to 0.5, r:{r}')

        #TODO: Optimize this method
        #-devisor is always the same
        #-delta3 = -delta1 -delta2
        #-precalculate r^3 and r^2 (this might make it more efficient?)
        beta1 = (12*r**3 + 14*r**2 + 21*r + 9)/(4*r**3 + 4*r**2 + 3*r - 3)
        beta2 = (16*r**3 + 8*r**2 - 12*r - 12)/(4*r**3 + 4*r**2 + 3*r - 3)
        beta3 = 0
        delta1=(12*r**3 + 9*r**2 + 3*r)/(4*r**3 + 4*r**2 + 3*r - 3)
        delta2=(8*r**4 - 24*r**3 - 36*r**2 - 12*r)/(4*r**3 + 4*r**2 + 3*r - 3)
        delta3=(-8*r**4 + 12*r**3 + 27*r**2 + 9*r)/(4*r**3 + 4*r**2 + 3*r - 3)

        return (beta1,beta2,delta1,delta2,delta3)
    '''

    def _adjust_h(self,err_mag,TOL = 1e-5,step_accept = True,itt = 0):
        #function to adjust the step size
        fac = 0.9 * (2*self.maxit+1)/(2*self.maxit+itt)
        return fac * self.h * err_mag ** (-1/4)



if __name__ == '__main__':
    #-----example usage-----
    from assimulo.ode import Explicit_Problem
    import matplotlib.pyplot as plt

    #define the problem
    g : float = 9.81
    l : float = 0.5
    theta : float = np.pi/5.
    def rhs(t,y):
        return np.array([
            y[1],
            -np.sin(y[0])*g/l
            ])

    def J(t,y):
        return np.array([
            [0, 1],
            [-np.cos(y[0])*g/l,0]
            ])

    #--helper functions for calculating period
    def arith_geo_mean(a,b):
        while True:
            a,b = (a+b)/2, np.sqrt(a*b)
            yield a,b

    from itertools import islice
    def elliptic_integral(k,tol=1e-5,maxiter=100):
        a_0,b_0 = 1.,np.sqrt(1-k**2)
        for a,b in islice(arith_geo_mean(a_0,b_0),maxiter):
            if abs(a-b) < tol:
                return np.pi /(2*a)
        else:
            raise Exception('Algorithm did not converge')

    periode = 4*np.sqrt(l/g) * elliptic_integral(np.sin(theta/2),tol=1e-10)

    #----exact solution
    v0 = [theta,0.]
    p = np.sqrt(g/l)
    def sol(t,t0):
        u = v0[0]*np.cos(p*(t-t0))+(1/p)*v0[1]*np.sin(p*(t-t0))
        v = -p*v0[0]*np.sin(p*(t-t0))+v0[1]*np.cos(p*(t-t0))
        return v0[0]*np.cos(p*t)#[u,v]

    #----Initialize Problem
    pend_model = Explicit_Problem(rhs,y0=np.array([theta,0.]))
    pend_model.name = 'Pendulum simulation'
    pend_model.jac = J
    pend_model.h = 0.01

    #----Initialize Solver
    sim = Lobatto2ODE(pend_model)
    sim.h = 0.016
    sim._do_comp_err = False
    sim._dynamic_step = False
    sim.maxit= 10
    #----Simulate the solver
    t,y = sim.simulate(periode)
    #----Plot results
    plt.plot(t,[sol(T,0) for T in t])
    plt.plot(t,y[:,0])
    plt.plot(t,y[:,1])
    plt.xlabel('time')
    plt.show()
